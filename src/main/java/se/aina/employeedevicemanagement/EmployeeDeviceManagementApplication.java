package se.aina.employeedevicemanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeDeviceManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmployeeDeviceManagementApplication.class, args);
    }

}
