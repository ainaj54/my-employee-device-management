package se.aina.employeedevicemanagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.aina.employeedevicemanagement.dataaccess.EmployeeRepository;
import se.aina.employeedevicemanagement.models.domain.Employee;
import se.aina.employeedevicemanagement.models.dto.EmployeeDeviceSummaryDto;
import se.aina.employeedevicemanagement.models.dto.FullTimeEmployeeDto;
import se.aina.employeedevicemanagement.models.dto.NewEmployeesDto;

import java.util.ArrayList;
import java.util.HashMap;

@RestController
@RequestMapping(value = "/api/employees")
public class employeeController {
    private EmployeeRepository employeeRepository;

    @Autowired
    public employeeController(EmployeeRepository employeeRepository){
        this.employeeRepository = employeeRepository;
    }

    @GetMapping("")
    public HashMap<Long, Employee> getAllEmployees(){
        return employeeRepository.getAllEmployees();
    }


    @GetMapping("/{id}")
    public ResponseEntity<Employee> getEmployee(@PathVariable long id){
        if(!employeeRepository.checkValidID(id)){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        if(employeeRepository.checkExistingEmployee(id)) {
            return new ResponseEntity<Employee>(employeeRepository.getEmployee(id), HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }


    @PostMapping("")
    public ResponseEntity<Employee> addEmployee(@RequestBody Employee newEmployee){
        if(employeeRepository.checkValidEmployee(newEmployee)) {
            employeeRepository.addEmployee(newEmployee);
            return new ResponseEntity<Employee>(employeeRepository.getEmployee(newEmployee.getEmployeeID()), HttpStatus.CREATED);
        }
        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Employee> deleteEmployee(@PathVariable long id){
        if(employeeRepository.checkExistingEmployee(id)){
            employeeRepository.removeEmployee(id);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

    }

    @PutMapping("/{id}")
    public ResponseEntity<Employee> switchEmployee(@PathVariable long id, @RequestBody Employee employee){
        if(!employeeRepository.checkValidEmployee(id, employee)){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        if(!employeeRepository.checkExistingEmployee(id)){
           employeeRepository.addEmployee(employee);
           return new ResponseEntity<Employee>(employeeRepository.getEmployee(id), HttpStatus.OK);
        }

        employeeRepository.switchEmployee(employee);
        return new ResponseEntity<>(employeeRepository.getEmployee(employee.getEmployeeID()), HttpStatus.OK);

    }

    @PatchMapping("/{id}")
    public ResponseEntity<Employee> modifyEmployee(@PathVariable long id,@RequestBody Employee employee){
        if(!employeeRepository.checkValidEmployee(id, employee)){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        if(!employeeRepository.checkExistingEmployee(id)){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        employeeRepository.modifyEmployee(employee);
        return new ResponseEntity<>(employeeRepository.getEmployee(employee.getEmployeeID()), HttpStatus.OK);

    }
    //Create function for adding/removing device/devices.

    @DeleteMapping("/devices/{id}")
    public ResponseEntity<Employee> removeAllDevices(@PathVariable long id){
        if(!employeeRepository.checkExistingEmployee(id)){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        employeeRepository.removeAllDevices(id);
        return new ResponseEntity<Employee>(employeeRepository.getEmployee(id), HttpStatus.OK);
    }

    @PatchMapping("/devices/{id}")
    public ResponseEntity<Employee> changeEmployeeDevices(@PathVariable long id, @RequestBody ArrayList<String> newDeviceList){
        if(!employeeRepository.checkExistingEmployee(id)){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Employee>(employeeRepository.switchDevices(id, newDeviceList), HttpStatus.OK);
    }

    @GetMapping("/devices/summary")
    public ResponseEntity<EmployeeDeviceSummaryDto> getEmployeeDeviceSummary(){
        if(employeeRepository.getAllEmployees().isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<EmployeeDeviceSummaryDto>(employeeRepository.getDeviceSummary(), HttpStatus.OK);
    }

    @GetMapping("/new")
    public  ResponseEntity<NewEmployeesDto> getNewEmployeeSummary(){
        if(employeeRepository.getAllEmployees().isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<NewEmployeesDto>(employeeRepository.getNewEmployees(), HttpStatus.OK);
    }

    @GetMapping("/birthdays")
    public ResponseEntity<FullTimeEmployeeDto> getFulltimeEmployeeBirthdaySummary(){
        if(employeeRepository.getAllEmployees().isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<FullTimeEmployeeDto>(employeeRepository.getFullTimeEmployeeBirthday(), HttpStatus.OK);

    }

}
