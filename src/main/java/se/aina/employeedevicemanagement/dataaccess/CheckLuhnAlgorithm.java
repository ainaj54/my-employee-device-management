package se.aina.employeedevicemanagement.dataaccess;

public class CheckLuhnAlgorithm {

    public static boolean checkIdLuhn(long inputId) {

        int[] inputDigitArray = Long.toString(inputId).chars().map(c -> c - '0').toArray();
        if(inputDigitArray.length !=  10){
            return false;
        }
        int sum = 0;
        int[] doubleDigitArray;
        int sumDoubleDigits;
        String doubleDigitString;
        for (int i = 0; i < inputDigitArray.length - 1; i += 2) {
            if (inputDigitArray[i] * 2 > 9) {

                doubleDigitArray = Long.toString(inputDigitArray[i] * 2).chars().map(c -> c - '0').toArray();
                sumDoubleDigits = doubleDigitArray[0] + doubleDigitArray[1];
                sum = sum + sumDoubleDigits + inputDigitArray[i + 1];
            } else {
                sum = sum +  (inputDigitArray[i] * 2) + inputDigitArray[i + 1];
            }
        }
        if (sum % 10 == 0) {
            return true;
        }
        return false;
    }
}