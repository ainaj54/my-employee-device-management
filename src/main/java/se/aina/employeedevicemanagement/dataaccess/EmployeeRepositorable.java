package se.aina.employeedevicemanagement.dataaccess;

import se.aina.employeedevicemanagement.models.domain.Employee;
import se.aina.employeedevicemanagement.models.dto.EmployeeDeviceSummaryDto;
import se.aina.employeedevicemanagement.models.dto.FullTimeEmployeeDto;
import se.aina.employeedevicemanagement.models.dto.NewEmployeesDto;

import java.util.ArrayList;
import java.util.HashMap;

public interface EmployeeRepositorable {
    HashMap<Long, Employee> getAllEmployees();
    Employee getEmployee(long id);
    Employee addEmployee(Employee newEmployee);
    void removeEmployee(long id);
    boolean checkValidID(long id);
    boolean checkValidEmployee(Employee employee);
    boolean checkValidEmployee(long id, Employee employee);
    boolean checkExistingEmployee(long id);
    Employee switchEmployee(Employee employee);
    Employee modifyEmployee(Employee employee);
    void removeAllDevices(long id);
    Employee switchDevices(long id, ArrayList<String> newDevices);
    EmployeeDeviceSummaryDto getDeviceSummary();
    NewEmployeesDto getNewEmployees();
    FullTimeEmployeeDto getFullTimeEmployeeBirthday();
}
