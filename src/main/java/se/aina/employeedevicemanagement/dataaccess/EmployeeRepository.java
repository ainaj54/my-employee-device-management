package se.aina.employeedevicemanagement.dataaccess;

import org.springframework.stereotype.Component;
import se.aina.employeedevicemanagement.models.domain.Employee;
import se.aina.employeedevicemanagement.models.domain.EmploymentPosition;
import se.aina.employeedevicemanagement.models.domain.EmploymentStatus;
import se.aina.employeedevicemanagement.models.dto.EmployeeDeviceSummaryDto;
import se.aina.employeedevicemanagement.models.dto.FullTimeEmployeeDto;
import se.aina.employeedevicemanagement.models.dto.NewEmployeesDto;
import se.aina.employeedevicemanagement.models.maps.EmployeeDeviceSummaryMapper;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

import static se.aina.employeedevicemanagement.dataaccess.CheckLuhnAlgorithm.checkIdLuhn;
import static se.aina.employeedevicemanagement.models.maps.EmployeeDeviceSummaryMapper.mapDeviceSummary;
import static se.aina.employeedevicemanagement.models.maps.FulltimeEmployeeMapper.mapFulltimeEmployeesBirthdays;
import static se.aina.employeedevicemanagement.models.maps.NewEmployeeMapper.mapNewEmployees;

@Component
public class EmployeeRepository implements EmployeeRepositorable {
    private HashMap<Long, Employee> employees = seedEmployees();

    private HashMap<Long, Employee> seedEmployees(){
        var employees = new HashMap<Long, Employee>();
        var johnDevices = new ArrayList<String>();
        johnDevices.add("Dell computer 13");
        var janeDevices = new ArrayList<String>();
        janeDevices.add("alienware X17");
        janeDevices.add("Samsung Monitor");


        employees.put(2018269734L, new Employee(2018269734, "John", "Doe", LocalDate.of(1980,02,14), LocalDate.of(2021,3,31), EmploymentStatus.PARTTIME, EmploymentPosition.TESTER, johnDevices));
        employees.put(1634751505L, new Employee(1634751505, "Jane", "Doe", LocalDate.of(1984,05,17), LocalDate.of(2018,12,1), EmploymentStatus.FULLTIME, EmploymentPosition.MANAGER, janeDevices));
        return employees;
    }

    @Override
    public HashMap<Long, Employee> getAllEmployees(){
        return employees;
    }

    @Override
    public Employee getEmployee(long ID){
        return employees.get(ID);
    }

    @Override
    public Employee addEmployee(Employee newEmployee){
        employees.put(newEmployee.getEmployeeID(),newEmployee);
        return getEmployee(newEmployee.getEmployeeID());
    }

    @Override
    public void removeEmployee(long ID){
        employees.remove(ID);

    }



    @Override
    public boolean checkValidID(long ID) {
        return checkIdLuhn(ID);
    }

    @Override
    public boolean checkValidEmployee(Employee employee) {
        return checkValidID(employee.getEmployeeID()) && employee.getFirstName() != null && employee.getLastName() != null
                && employee.getDateOfBirth() != null && employee.getDateEmployed() != null && employee.getEmploymentPosition() != null
                && employee.getEmploymentStatus() != null;


    }

    @Override
    public boolean checkValidEmployee(long id, Employee employee) {

        return (id == employee.getEmployeeID()) && checkValidEmployee(employee);
    }


    @Override
    public boolean checkExistingEmployee(long id) {
        if(employees.containsKey(id)) {
            return true;
        }
        return false;
    }

    @Override
    public Employee switchEmployee(Employee employee) {
        employees.remove(employee.getEmployeeID());
        employees.put(employee.getEmployeeID(), employee);
        return employees.get(employee.getEmployeeID());
    }

    @Override
    public Employee modifyEmployee(Employee employee) {
        Employee employeeToModify = employees.get(employee.getEmployeeID());
        employeeToModify.setDateEmployed(employee.getDateEmployed());
        employeeToModify.setEmploymentStatus(employee.getEmploymentStatus());
        employeeToModify.setEmploymentPosition(employee.getEmploymentPosition());
        employeeToModify.setDateOfBirth(employee.getDateOfBirth());
        employeeToModify.setFirstName(employee.getFirstName());
        employeeToModify.setLastName(employee.getLastName());
        employeeToModify.setCurrentDevices(employee.getCurrentDevices());
        return employeeToModify;
    }

    @Override
    public void removeAllDevices(long id) {
        var devices = getEmployee(id).getCurrentDevices();
        devices.clear();
    }

    @Override
    public Employee switchDevices(long id, ArrayList<String> newDevices) {
        getEmployee(id).setCurrentDevices(newDevices);
        return getEmployee(id);
    }

    @Override
    public EmployeeDeviceSummaryDto getDeviceSummary() {
        return mapDeviceSummary(employees);
    }

    @Override
    public NewEmployeesDto getNewEmployees() {
        return mapNewEmployees(employees);
    }

    @Override
    public FullTimeEmployeeDto getFullTimeEmployeeBirthday() {
        return mapFulltimeEmployeesBirthdays(employees);
    }


}
