package se.aina.employeedevicemanagement.dataaccess;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class ReadObjectFromFile {
    public static ArrayList<Object> ReadObjectFromFile(){
        String path = "employeesData.txt";

        ArrayList<Object> objectList = new ArrayList<>();
        Boolean cont = true;

        while(cont){
            try( FileInputStream fileInputStream = new FileInputStream(path);
                 ObjectInputStream input = new ObjectInputStream(fileInputStream)){
                Object obj = input.readObject();
                if(obj != null){
                    objectList.add(obj);
                }else{
                    cont = false;
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        return objectList;
    }
}
