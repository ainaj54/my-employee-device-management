package se.aina.employeedevicemanagement.models.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

public class Employee {
    private long employeeID;
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;
    private LocalDate dateEmployed;
    private EmploymentStatus employmentStatus;
    private EmploymentPosition employmentPosition;
    private ArrayList<String> currentDevices;


    public Employee(long employeeID, String firstName, String lastName, LocalDate dateOfBirth, LocalDate dateEmployed, EmploymentStatus employmentStatus, EmploymentPosition employmentPosition, ArrayList<String> currentDevices) {
        this.employeeID = employeeID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.dateEmployed = dateEmployed;
        this.employmentStatus = employmentStatus;
        this.employmentPosition = employmentPosition;
        this.currentDevices = currentDevices;
    }

    public long getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(long employeeID) {
        this.employeeID = employeeID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public LocalDate getDateEmployed() {
        return dateEmployed;
    }

    public void setDateEmployed(LocalDate dateEmployed) {
        this.dateEmployed = dateEmployed;
    }

    public EmploymentStatus getEmploymentStatus() {
        return employmentStatus;
    }

    public void setEmploymentStatus(EmploymentStatus employmentStatus) {
        this.employmentStatus = employmentStatus;
    }

    public EmploymentPosition getEmploymentPosition() {
        return employmentPosition;
    }

    public void setEmploymentPosition(EmploymentPosition employmentPosition) {
        this.employmentPosition = employmentPosition;
    }

    public ArrayList<String> getCurrentDevices() {
        return currentDevices;
    }

    public void setCurrentDevices(ArrayList<String> currentDevices) {
        this.currentDevices = currentDevices;
    }

    public void addDevices(String newDevice){
        currentDevices.add(newDevice);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return employeeID == employee.employeeID && Objects.equals(firstName, employee.firstName) && Objects.equals(lastName, employee.lastName) && Objects.equals(dateOfBirth, employee.dateOfBirth) && Objects.equals(dateEmployed, employee.dateEmployed) && Objects.equals(employmentStatus, employee.employmentStatus) && Objects.equals(employmentPosition, employee.employmentPosition) && Objects.equals(currentDevices, employee.currentDevices);
    }

    @Override
    public int hashCode() {
        return Objects.hash(employeeID, firstName, lastName, dateOfBirth, dateEmployed, employmentStatus, employmentPosition, currentDevices);
    }
}
