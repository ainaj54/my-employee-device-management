package se.aina.employeedevicemanagement.models.domain;

public enum EmploymentPosition {
    DEVELOPER,
    TESTER,
    MANAGER;

}
