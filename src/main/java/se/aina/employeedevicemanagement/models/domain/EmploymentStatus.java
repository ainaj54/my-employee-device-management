package se.aina.employeedevicemanagement.models.domain;

public enum EmploymentStatus {
    PARTTIME,
    FULLTIME
}
