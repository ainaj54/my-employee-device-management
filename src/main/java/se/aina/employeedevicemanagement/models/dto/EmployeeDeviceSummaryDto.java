package se.aina.employeedevicemanagement.models.dto;

import se.aina.employeedevicemanagement.models.maps.EmployeeDevices;

import java.util.HashMap;

public class EmployeeDeviceSummaryDto {
    private HashMap<Long, EmployeeDevices> employeeDevicesSummary;


    public EmployeeDeviceSummaryDto(HashMap<Long, EmployeeDevices> employeeDevicesSummary) {
        this.employeeDevicesSummary = employeeDevicesSummary;
    }

    public HashMap<Long, EmployeeDevices> getEmployeeDevicesSummary() {
        return employeeDevicesSummary;
    }

    public void setEmployeeDevicesSummary(HashMap<Long, EmployeeDevices> employeeDevicesSummary) {
        this.employeeDevicesSummary = employeeDevicesSummary;
    }
}
