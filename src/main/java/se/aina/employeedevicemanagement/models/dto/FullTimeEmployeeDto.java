package se.aina.employeedevicemanagement.models.dto;

import java.util.ArrayList;

public class FullTimeEmployeeDto {
    ArrayList<String> fullTimeEmployeeBirthdays;

    public FullTimeEmployeeDto(ArrayList<String> fullTimeEmployeeBirthdays) {
        this.fullTimeEmployeeBirthdays = fullTimeEmployeeBirthdays;
    }

    public ArrayList<String> getFullTimeEmployeeBirthdays() {
        return fullTimeEmployeeBirthdays;
    }

    public void setFullTimeEmployeeBirthdays(ArrayList<String> fullTimeEmployeeBirthdays) {
        this.fullTimeEmployeeBirthdays = fullTimeEmployeeBirthdays;
    }
}
