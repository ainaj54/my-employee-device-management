package se.aina.employeedevicemanagement.models.dto;

import java.util.ArrayList;

public class NewEmployeesDto {
    public ArrayList<String> getNewEmployees() {
        return newEmployees;
    }

    public void setNewEmployees(ArrayList<String> newEmployees) {
        this.newEmployees = newEmployees;
    }

    private ArrayList<String> newEmployees;

    public NewEmployeesDto(ArrayList<String> newEmployees) {
        this.newEmployees = newEmployees;
    }
}
