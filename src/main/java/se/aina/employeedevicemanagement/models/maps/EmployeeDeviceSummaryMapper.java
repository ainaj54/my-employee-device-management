package se.aina.employeedevicemanagement.models.maps;

import se.aina.employeedevicemanagement.models.domain.Employee;
import se.aina.employeedevicemanagement.models.dto.EmployeeDeviceSummaryDto;

import java.util.HashMap;

public class EmployeeDeviceSummaryMapper {
    public static EmployeeDeviceSummaryDto mapDeviceSummary(HashMap<Long, Employee> employees){
        HashMap<Long, EmployeeDevices> employeeDevicesSummary = new HashMap<>();

        for(long key : employees.keySet()){
            var employeeName = employees.get(key).getFirstName() + " " +employees.get(key).getLastName();
            var numDevices = employees.get(key).getCurrentDevices().size();
            EmployeeDevices employeeDevices = new EmployeeDevices(employeeName, numDevices);

            employeeDevicesSummary.put(key, employeeDevices);
        }

        return new EmployeeDeviceSummaryDto(employeeDevicesSummary);
    }


}
