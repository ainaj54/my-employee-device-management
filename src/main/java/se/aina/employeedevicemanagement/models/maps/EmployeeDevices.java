package se.aina.employeedevicemanagement.models.maps;

public class EmployeeDevices {
    private String employeeName;
    private int numDevices;

    public EmployeeDevices(String employeeName, int numDevices) {
        this.employeeName = employeeName;
        this.numDevices = numDevices;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public int getNumDevices() {
        return numDevices;
    }

    public void setNumDevices(int numDevices) {
        this.numDevices = numDevices;
    }
}
