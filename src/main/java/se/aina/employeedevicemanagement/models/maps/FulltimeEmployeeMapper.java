package se.aina.employeedevicemanagement.models.maps;

import se.aina.employeedevicemanagement.models.domain.Employee;
import se.aina.employeedevicemanagement.models.domain.EmploymentStatus;
import se.aina.employeedevicemanagement.models.dto.FullTimeEmployeeDto;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;

public class FulltimeEmployeeMapper {
    public static FullTimeEmployeeDto mapFulltimeEmployeesBirthdays(HashMap<Long, Employee> employees){
        ArrayList<String> fulltimeEmployeeBirthdays = new ArrayList<>();


        for(Employee employee: employees.values()){
            if(employee.getEmploymentStatus() == EmploymentStatus.FULLTIME){
                LocalDate employeeBirthday = LocalDate.of(LocalDate.now().getYear(),employee.getDateOfBirth().getMonth(),employee.getDateOfBirth().getDayOfMonth());
                long daysUntilBirthday = LocalDate.now().until(employeeBirthday, ChronoUnit.DAYS);

                fulltimeEmployeeBirthdays.add(employee.getFirstName() + " " + employee.getLastName() + "'s birthday (" +
                        employee.getDateOfBirth() + ") is in " + daysUntilBirthday + " days." );
            }

        }

        return new FullTimeEmployeeDto(fulltimeEmployeeBirthdays);
    }
}
