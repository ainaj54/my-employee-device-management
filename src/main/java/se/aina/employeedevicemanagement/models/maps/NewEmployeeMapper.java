package se.aina.employeedevicemanagement.models.maps;

import se.aina.employeedevicemanagement.dataaccess.EmployeeRepository;
import se.aina.employeedevicemanagement.models.domain.Employee;
import se.aina.employeedevicemanagement.models.dto.NewEmployeesDto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

public class NewEmployeeMapper {
    public static NewEmployeesDto mapNewEmployees(HashMap<Long, Employee> employees){
        ArrayList<String> newEmployees = new ArrayList<>();



        for(Employee employee: employees.values()){
            if(employee.getDateEmployed().isAfter(LocalDate.now().minusYears(2))){
                newEmployees.add( employee.getEmployeeID() + ": " + employee.getFirstName() + " " + employee.getLastName());
            }
        }

        if(newEmployees.isEmpty()){
            newEmployees.add("No new employees in repository.");
        }

        return new NewEmployeesDto(newEmployees);
    }
}
