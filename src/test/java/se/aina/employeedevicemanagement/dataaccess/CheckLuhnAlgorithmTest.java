package se.aina.employeedevicemanagement.dataaccess;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static se.aina.employeedevicemanagement.dataaccess.CheckLuhnAlgorithm.checkIdLuhn;

class CheckLuhnAlgorithmTest {

    @Test
    void checkLuhn_validNumber() {
        long validLuhnNumber = 6784179910L;
        assertTrue(checkIdLuhn(validLuhnNumber));

    }

    @Test
    void checkIdLuhn_invalidNumber(){
        long invalidLuhnNumber = 6784179911L;
        assertFalse(checkIdLuhn(invalidLuhnNumber));
    }

    @Test
    void checkIdLuhn_InvalidLength(){
        long incorrectlengthNumber = 635489789L;
        assertFalse(checkIdLuhn(incorrectlengthNumber));
    }
    @Test
    void checkIdLuhn_ValidLength(){
        long correctlengthNumber = 6784179910L;
        assertTrue(checkIdLuhn(correctlengthNumber));
    }

}