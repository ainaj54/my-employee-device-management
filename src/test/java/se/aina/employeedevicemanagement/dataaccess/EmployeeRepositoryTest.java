package se.aina.employeedevicemanagement.dataaccess;

import org.junit.jupiter.api.Test;
import se.aina.employeedevicemanagement.models.domain.Employee;
import se.aina.employeedevicemanagement.models.domain.EmploymentPosition;
import se.aina.employeedevicemanagement.models.domain.EmploymentStatus;

import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeRepositoryTest {

    @Test
    void getEmployee_existing_employee() {
        EmployeeRepository employeeRepository = new EmployeeRepository();
        var johnDevices = new ArrayList<String>();
        johnDevices.add("Dell computer 13");
        Employee expected = new Employee(2018269734, "John", "Doe", LocalDate.of(1980,02,14), LocalDate.of(2021,3,31), EmploymentStatus.PARTTIME, EmploymentPosition.TESTER, johnDevices);
        var actual = employeeRepository.getEmployee(2018269734);

        assertEquals(expected, actual);

    }

    @Test
    void getGetEmployee_nonexisting_employee(){
        EmployeeRepository employeeRepository = new EmployeeRepository();
        long employeeID = 3546L;

        assertNull(employeeRepository.getEmployee(employeeID));
    }

    @Test
    void addEmployee_acceptscorrectformat(){
        EmployeeRepository employeeRepository = new EmployeeRepository();

        ArrayList<String> svenDevices = new ArrayList<>();
        svenDevices.add("Dell XPS 15");
        svenDevices.add("Samsung 28-inch monitor");
        Employee expected = new Employee(1507119608, "Sven", "Svensson", LocalDate.of(1980,02,14), LocalDate.of(2019,2,15),EmploymentStatus.FULLTIME, EmploymentPosition.DEVELOPER, svenDevices);
        Employee actual = employeeRepository.addEmployee(expected);

        assertEquals(expected, actual);
        
    }

    @Test
    void checkValidEmployeeObject_validEmployee(){
        EmployeeRepository employeeRepository = new EmployeeRepository();

        ArrayList<String> svenDevices = new ArrayList<>();
        svenDevices.add("Dell XPS 15");
        svenDevices.add("Samsung 28-inch monitor");
        Employee employee = new Employee(1507119608, "Sven", "Svensson", LocalDate.of(1980,02,14),LocalDate.of(2019,2,15),EmploymentStatus.FULLTIME, EmploymentPosition.DEVELOPER, svenDevices);
        assertTrue(employeeRepository.checkValidEmployee(employee));
    }

    @Test
    void checkValidEmployeeObject_invalidEmployee(){
        EmployeeRepository employeeRepository = new EmployeeRepository();

        ArrayList<String> svenDevices = new ArrayList<>();
        svenDevices.add("Dell XPS 15");
        svenDevices.add("Samsung 28-inch monitor");
        Employee employee = new Employee(1507119608, "Sven", null, LocalDate.of(1980,02,14), LocalDate.of(2019,2,15),EmploymentStatus.FULLTIME, EmploymentPosition.DEVELOPER, svenDevices);
        assertFalse(employeeRepository.checkValidEmployee(employee));
    }

    @Test
    void checkIfEmployeeExistInRepository_existingEmployee(){
        EmployeeRepository employeeRepository = new EmployeeRepository();
        long id = 2018269734;

        assertTrue(employeeRepository.checkExistingEmployee(id));
    }

    @Test
    void checkIfEmployeeExistInRepository_nonExistingEmployee(){
        EmployeeRepository employeeRepository = new EmployeeRepository();
        long id = 1234567891;
        assertFalse(employeeRepository.checkExistingEmployee(id));

    }

    @Test
    void checkEmployeeIDMatchWithInputID_validID(){
        EmployeeRepository employeeRepository = new EmployeeRepository();
        ArrayList<String> svenDevices = new ArrayList<>();
        svenDevices.add("Dell XPS 15");
        svenDevices.add("Samsung 28-inch monitor");
        Employee employee = new Employee(8452608394L, "Sven", "Svensson", LocalDate.of(1980,02,14), LocalDate.of(2019,2,15),EmploymentStatus.FULLTIME, EmploymentPosition.DEVELOPER, svenDevices);
        assertTrue(employeeRepository.checkValidEmployee(8452608394L,employee));

    }

    @Test
    void checkEmployeeIDMatchWithInputID_invalidID(){
        EmployeeRepository employeeRepository = new EmployeeRepository();
        ArrayList<String> svenDevices = new ArrayList<>();
        svenDevices.add("Dell XPS 15");
        svenDevices.add("Samsung 28-inch monitor");
        Employee employee = new Employee(8452608394L, "Sven", "Svensson", LocalDate.of(1980,02,14), LocalDate.of(2019,2,15),EmploymentStatus.FULLTIME, EmploymentPosition.DEVELOPER, svenDevices);
        assertFalse(employeeRepository.checkValidEmployee(1507119708,employee));

    }

    @Test
    void checkEmployeeIDMatchWithInputID_invalidEmployee(){
        EmployeeRepository employeeRepository = new EmployeeRepository();
        ArrayList<String> svenDevices = new ArrayList<>();
        svenDevices.add("Dell XPS 15");
        svenDevices.add("Samsung 28-inch monitor");
        Employee employee = new Employee(8452608394L, "Sven", null, LocalDate.of(1980,02,14),LocalDate.of(2019,2,15),EmploymentStatus.FULLTIME, EmploymentPosition.DEVELOPER, svenDevices);
        assertFalse(employeeRepository.checkValidEmployee(1507119708,employee));

    }

    @Test
    void switchEmployeeInRepository_valid(){
        EmployeeRepository employeeRepository = new EmployeeRepository();
        ArrayList<String> svenDevices = new ArrayList<>();
        svenDevices.add("Dell XPS 15");
        svenDevices.add("Samsung 28-inch monitor");
        Employee expected = new Employee(2018269734L, "Sven", "Svensson", LocalDate.of(1980,02,14),LocalDate.of(2019,2,15),EmploymentStatus.FULLTIME, EmploymentPosition.DEVELOPER, svenDevices);

        Employee actual = employeeRepository.switchEmployee(expected);

        assertEquals(expected, actual);

    }

    @Test
    void modifyExistingEmployee(){
        EmployeeRepository employeeRepository = new EmployeeRepository();
        var johnDevices = new ArrayList<String>();
        johnDevices.add("Dell computer 13");
        Employee expected = new Employee(2018269734, "John", "Doe", LocalDate.of(1980,02,14), LocalDate.of(2021,3,31), EmploymentStatus.PARTTIME, EmploymentPosition.TESTER, johnDevices);

        Employee actual = employeeRepository.modifyEmployee(expected);

        assertEquals(expected, actual);

    }

    @Test
    void RemoveDeviceList(){
        EmployeeRepository employeeRepository = new EmployeeRepository();
        long id = 2018269734;
        employeeRepository.removeAllDevices(id);
        var expected = new ArrayList<String>();
        var actual = employeeRepository.getEmployee(2018269734).getCurrentDevices();

        assertEquals(expected,actual);

    }

    @Test
    void switchDevices(){
        EmployeeRepository employeeRepository = new EmployeeRepository();
        long id = 2018269734;
        ArrayList<String> expected = new ArrayList<>();
        expected.add("new monitor");
        expected.add("old Phone");

        employeeRepository.switchDevices(id, expected);


        var actual = employeeRepository.getEmployee(2018269734).getCurrentDevices();

        assertEquals(expected,actual);
    }



    }